from  rest_framework import serializers
from  twitterbackend.models import Message

class MessageSerializers(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'
