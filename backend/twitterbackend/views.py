from rest_framework import generics
from .serializers import MessageSerializers
from .models import Message

class MessageCreateView(generics.CreateAPIView):
    serializer_class = MessageSerializers

class MessageListView(generics.ListAPIView):
    serializer_class = MessageSerializers
    queryset = Message.objects.all().order_by('-id')